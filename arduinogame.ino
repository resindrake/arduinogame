#include <LiquidCrystal.h>
#include <EEPROM.h>
#include "Pitches.h"

// Analog pin inputs
const int analog_x_pin = A0; // X coord of analog stick
const int analog_y_pin = A1; // Y coord of analog stick

// Digital pin inputs
const int analog_press_pin = 2; // Digital button press on analog stick (INPUT_PULLUP)
const int red_button_pin = 3; // The red button (INPUT_PULLUP)
const int blue_button_pin = 4; // The blue button (INPUT_PULLUP)
const int screen_RS = 5;
const int screen_EN = 6;
const int screen_D4 = 7;
const int screen_D5 = 8;
const int screen_D6 = 9;
const int screen_D7 = 10;
const int speaker_pin = 11; // Speaker pin (OUTPUT)
const int start_button_pin = 12; // The start button (INPUT_PULLUP)


// LCD Screen
LiquidCrystal lcd(screen_RS, screen_EN, screen_D4, screen_D5, screen_D6, screen_D7);

// Button states
int red_state = 0;
int blue_state = 0;
int stick_state = 0;
int start_state = 0;

#define DEBUG 1


// Stick coords
int stick_x = 0; //+up -down
int stick_y = 7; //+right -left

// Stick deadzones
int stick_x_deadzone_plus = 20;
int stick_x_deadzone_minus = 20;

int stick_y_deadzone_plus = 20;
int stick_y_deadzone_minus = 20;

// Other vars
int lives;
int score;
//int highscore = 1024;
int highscore;
int eeAddress = 0;


bool shot_exists;
int gameSpeed = 100;
int world_data[16][4];
int player_x = 0;
int player_y = 0;
int shot_x;
int shot_y;
int milestone;
long displayLivesUntil;
long displayScoreUntil;
int nextShotAvailable;
bool bombAvailable;
bool bombTick;

enum {
	start_menu,
	game,
	pause_menu,
	debug_1,
	debug_2
} mode;


// Screen chars

int BlockUpper = 0;
byte BlockUpperBytes[8] = {
    0b11111,
    0b11111,
    0b11111,
    0b11111,
    0b00000,
    0b00000,
    0b00000,
    0b00000
};

int BlockLower = 1;
byte BlockLowerBytes[8] = {
    0b00000,
    0b00000,
    0b00000,
    0b00000,
    0b11111,
    0b11111,
    0b11111,
    0b11111
};

int BlockBoth = 2;
byte BlockBothBytes[8] = {
    0b11111,
    0b11111,
    0b11111,
    0b11111,
    0b11111,
    0b11111,
    0b11111,
    0b11111
};

int PlayerUpper = 3;
byte PlayerUpperBytes[8] = {
    0b11110,
    0b01111,
    0b01111,
    0b11110,
    0b00000,
    0b00000,
    0b00000,
    0b00000
};

int PlayerLower = 4;
byte PlayerLowerBytes[8] = {
    0b00000,
    0b00000,
    0b00000,
    0b00000,
    0b11110,
    0b01111,
    0b01111,
    0b11110
};

int BlockUpperPlayerLower = 5;
byte BlockUpperPlayerLowerBytes[8] = {
    0b11111,
    0b11111,
    0b11111,
    0b11111,
    0b11110,
    0b01111,
    0b01111,
    0b11110
};

int PlayerUpperBlockLower = 6;
byte PlayerUpperBlockLowerBytes[8] = {
    0b11110,
    0b01111,
    0b01111,
    0b11110,
    0b11111,
    0b11111,
    0b11111,
    0b11111
};

// Functions for getting a nicer state of the buttons
bool red_pressed() {
    return (red_state == 2);
}
bool blue_pressed() {
    return (blue_state == 2);
}
bool stick_pressed() {
    return (stick_state == 2);
}
bool start_pressed() {
    return (start_state == 2);
}

bool red_released() {
    return (red_state == -1);
}
bool blue_released() {
    return (blue_state == -1);
}
bool stick_released() {
    return (stick_state == -1);
}
bool start_released() {
    return (start_state == -1);
}

bool red_held() {
    return (red_state >= 1);
}
bool blue_held() {
    return (blue_state >= 1);
}
bool stick_held() {
    return (stick_state >= 1);
}
bool start_held() {
    return (start_state >= 1);
}

bool red_unheld() {
    return (red_state <= 0);
}
bool blue_unheld() {
    return (blue_state <= 0);
}
bool stick_unheld() {
    return (stick_state <= 0);
}
bool start_unheld() {
    return (start_state <= 0);
}









void setup() {
	#ifdef DEBUG
	Serial.begin(9600);
	#endif
    lcd.begin(16, 2);
    pinMode(analog_press_pin, INPUT_PULLUP);
    pinMode(red_button_pin, INPUT_PULLUP);
    pinMode(blue_button_pin, INPUT_PULLUP);
    pinMode(start_button_pin, INPUT_PULLUP);
    pinMode(speaker_pin, OUTPUT);
    lcd.createChar(PlayerUpper, PlayerUpperBytes);
    lcd.createChar(PlayerLower, PlayerLowerBytes);
    lcd.createChar(PlayerUpperBlockLower, PlayerUpperBlockLowerBytes);

    lcd.createChar(BlockUpper, BlockUpperBytes);
    lcd.createChar(BlockLower, BlockLowerBytes);
    lcd.createChar(BlockBoth, BlockBothBytes);
    lcd.createChar(BlockUpperPlayerLower, BlockUpperPlayerLowerBytes);
    EEPROM.get(eeAddress, highscore);

    mode = start_menu; // Start screen
    randomSeed(analogRead(A2)); //This stops the world from being the same on each init. I tested without; it would literally generate the same random world each time; it was great!
}

// Main program loop. Can be cycled between various modes.
void loop() {

    // Determine whether the buttons are fresh (pressed just this loop)
    int red_is_fresh = (red_state <= 0);
    int blue_is_fresh = (blue_state <= 0);
    int stick_is_fresh = (stick_state <= 0);
    int start_is_fresh = (start_state <= 0);

    // Get button states (and invert the pullup of digital)
    red_state = !digitalRead(red_button_pin);
    blue_state = !digitalRead(blue_button_pin);
    stick_state = !digitalRead(analog_press_pin);
    start_state = !digitalRead(start_button_pin);
    stick_x = analogRead(analog_x_pin) - 512;
    stick_y = analogRead(analog_y_pin) - 512;

    // Add edge data to button (2 = pressed this tick, -1 = released this tick)
    if (red_state > 0 && red_is_fresh) red_state = 2;
    if (blue_state > 0 && blue_is_fresh) blue_state = 2;
    if (stick_state > 0 && stick_is_fresh) stick_state = 2;
    if (start_state > 0 && start_is_fresh) start_state = 2;
    if (red_state == 0 && !red_is_fresh) red_state = -1;
    if (blue_state == 0 && !blue_is_fresh) blue_state = -1;
    if (stick_state == 0 && !stick_is_fresh) stick_state = -1;
    if (start_state == 0 && !start_is_fresh) start_state = -1;

    // Apply deadzone to analog stick
    if (stick_x < stick_x_deadzone_plus && stick_x > -stick_x_deadzone_minus) stick_x = 0;
    if (stick_y < stick_y_deadzone_plus && stick_y > -stick_y_deadzone_minus) stick_y = 0;

    // Now do whatever we're supposed to be doing
    switch (mode) {
        case start_menu:	start_menu_loop(); break;
        case game: 			game_loop(); break;
        case pause_menu: 	pause_menu_loop(); break;
        case debug_1: 		debug_1_loop(); break;
        case debug_2: 		debug_2_loop(); break;
    }
}






void start_menu_loop() {
    lcd.clear();
    if (millis() % 1000 >= 500) {
        lcd.setCursor(5, 0);
        lcd.print("PRESS");
        lcd.setCursor(5, 1);
        lcd.print("START!");
    }

    delay(10);

    if (start_pressed() || red_pressed()) {
        game_setup(); // Resets the game vars
        mode = game; // Resumes the game
    }
    else if (blue_pressed()){
    	lcd.clear();
    		    
    	lcd.setCursor(0, 0);
	    lcd.print("PREVIOUS");
		lcd.setCursor(0, 1);
	    lcd.print(score);
	    
	    lcd.setCursor(12, 0);
	    lcd.print("BEST");
		lcd.setCursor(15 - (int) log10(highscore), 1);
	    lcd.print(highscore);
    	
	    delay(5000);
    }
    else if (stick_pressed()) mode = debug_1; // Debug menu
}


void game_setup() {
    player_x = 1;
    player_y = 1;
    shot_x = 0;
    shot_y = 0;
    shot_exists = false;
    lives = 3;
    milestone = 0;
    displayLivesUntil = millis() + 1500;
    displayScoreUntil = millis() + 1500;
    nextShotAvailable = 4;
    bombAvailable = true;
    bombTick = false;
    score = 0;
    clear_world();
}

void game_loop() {
	score++;
	nextShotAvailable--;

	check_player_collision();
    // Up/down/left/right movement (The stick's X and Y are oriented different to the screen's hence the vars appearing mixed up)
    if (stick_y < 0) player_x = max(0, player_x - 1); // Left
    else if (stick_y > 0) player_x = min(15, player_x + 1); // Right
    if (stick_x > 0) player_y = max(0, player_y - 1); // Up
    else if (stick_x < 0) player_y = min(3, player_y + 1); //Down
    check_player_collision();

    // Fire a shot
    if (red_pressed() && nextShotAvailable <= 0) {
        tone(speaker_pin, NOTE_DS8, 50);
        shot_x = player_x;
        shot_y = player_y;
        shot_exists = true;
        nextShotAvailable = 16;
        #ifdef DEBUG
		Serial.println("Shot fired");
		#endif
    }

    // Activate a screen clearing bomb
    if (blue_pressed() && bombAvailable) {
        clear_world();
        tone(speaker_pin, NOTE_C8, 200);
        bombAvailable = false; // Regen based on score hitting a multiple of 100, rather than being baseds on time
        bombTick = true;
        #ifdef DEBUG
		Serial.println("Bomb fired");
		#endif
    }
    
	if (shot_exists) {
		check_shot_collision();
        shot_x++;
	    check_shot_collision();
	}
	
	if (score > milestone + 100) { // Run every 100pts
        gameSpeed = max(10, gameSpeed - 1);
        displayScoreUntil = millis() + 1500;
        tone(speaker_pin, NOTE_B5, 50);
        bombAvailable = true;
        milestone+=100;
    }
	if (nextShotAvailable == 0) { // Play a tone when next shot is ready
		tone(speaker_pin, NOTE_A7, 50);
	}
	
    if (lives > 0) { //Alive
        draw_world();
        advance_world();
    } else { // Dead
        GameOver();
    }

	if (start_pressed()) mode = pause_menu; // Pause
	
    delay(gameSpeed);
}

void check_shot_collision() {
	if (shot_exists){
	    if (shot_x > 15) shot_exists = false; //Disables the shot if it goes off the world
	    else if (world_data[shot_x][shot_y]) { // Handles collision
	        tone(speaker_pin, NOTE_C8, 50);
	        world_data[shot_x][shot_y] = 0; // Deletes anything the shot hits
	        shot_exists = false; // Deletes the shot
	        score += 10; // Award 10pts for hitting a wall
	        #ifdef DEBUG
			Serial.println("Shot hit wall");
			#endif
	    }
    }
}

void check_player_collision() {
	if (world_data[player_x][player_y]) {
        tone(speaker_pin, NOTE_B3, 50);
        lives--;
        clear_world();
        player_x = 1;
        player_y = 1;
        displayLivesUntil = millis() + 3000;
        #ifdef DEBUG
		Serial.println("Lost life");
		#endif
    }
}

void clear_world() {
    for (int y = 0; y < 4; y++) {
        for (int x = 0; x < 16; x++) {
            world_data [x][y] = 0;
        }
    }
}

void advance_world() {
    for (int i = 0; i < 15; i++) {
        world_data[i][0] = world_data[i + 1][0];
        world_data[i][1] = world_data[i + 1][1];
        world_data[i][2] = world_data[i + 1][2];
        world_data[i][3] = world_data[i + 1][3];
    }
    if (random(8) == 7) {
        do {
            world_data[15][0] = random(2);
            world_data[15][1] = random(2);
            world_data[15][2] = random(2);
            world_data[15][3] = random(2);
        } while (world_data[15][0] + world_data[15][1] + world_data[15][2] + world_data[15][3] == 4);
    }
    else {
        world_data[15][0] = 0;
        world_data[15][1] = 0;
        world_data[15][2] = 0;
        world_data[15][3] = 0;
    }
}

void draw_world() {
    lcd.clear();
    if (bombTick) {
        for (int cursor_y = 0; cursor_y < 2; cursor_y++) {
            for (int cursor_x = 0; cursor_x < 16; cursor_x++) {
                lcd.setCursor(cursor_x, cursor_y);
                lcd.write(byte(BlockBoth));
            }
        }
        bombTick = false;
    }
    else {
        for (int cursor_y = 0; cursor_y < 2; cursor_y++) {
            for (int cursor_x = 0; cursor_x < 16; cursor_x++) {
                lcd.setCursor(cursor_x, cursor_y);
                if      (world_data[cursor_x][2 * cursor_y + 1] == 0 && ((player_x == cursor_x && player_y == 2*cursor_y)   || (shot_exists && shot_x == cursor_x && shot_y == 2*cursor_y)))   lcd.write(byte(PlayerUpper));
                else if (world_data[cursor_x][2 * cursor_y]     == 0 && ((player_x == cursor_x && player_y == 2*cursor_y+1) || (shot_exists && shot_x == cursor_x && shot_y == 2*cursor_y+1))) lcd.write(byte(PlayerLower));
                else if (world_data[cursor_x][2 * cursor_y + 1] == 1 && ((player_x == cursor_x && player_y == 2*cursor_y)   || (shot_exists && shot_x == cursor_x && shot_y == 2*cursor_y)))   lcd.write(byte(PlayerUpperBlockLower));
                else if (world_data[cursor_x][2 * cursor_y]     == 1 && ((player_x == cursor_x && player_y == 2*cursor_y+1) || (shot_exists && shot_x == cursor_x && shot_y == 2*cursor_y+1))) lcd.write(byte(BlockUpperPlayerLower));
                else if (world_data[cursor_x][2 * cursor_y]     == 1 && world_data[cursor_x][2 * cursor_y + 1] == 0) lcd.write(byte(BlockUpper));
                else if (world_data[cursor_x][2 * cursor_y]     == 0 && world_data[cursor_x][2 * cursor_y + 1] == 1) lcd.write(byte(BlockLower));
                else if (world_data[cursor_x][2 * cursor_y]     == 1 && world_data[cursor_x][2 * cursor_y + 1] == 1) lcd.write(byte(BlockBoth));
                else lcd.print(" ");
            }
        }
        if (millis() < displayLivesUntil) {
            lcd.setCursor(15, 0);
            lcd.print(lives);
        }
        if (millis() < displayScoreUntil) {
            lcd.setCursor(15 - (int) log10(milestone), 1);
            lcd.print(milestone);
        }
    }
}

void GameOver() {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("GAME OVER!");
    lcd.setCursor(0, 1);
    lcd.print("SCORE: ");
    lcd.setCursor(7, 1);
    lcd.print(score);
    delay(50);
    tone(speaker_pin, NOTE_B3, 50);
    delay(50);
    tone(speaker_pin, NOTE_B3, 50);
    if (score > highscore) {
    	highscore = score;
		EEPROM.put(eeAddress, highscore);
    	delay(1000);
    	for (int i = 0; i < 4; i++){
	    	lcd.setCursor(0, 0);
	    	lcd.print("NEW HIGH SCORE!");
	    	delay(500);
	    	lcd.setCursor(0, 0);
	    	lcd.print("                ");
	    	delay(500);
    	}
    	
    } else delay(5000);
    
    mode = start_menu; // Start menu
}










void pause_menu_loop() {
    lcd.clear();
    if (millis() % 1000 >= 500) {
	    lcd.setCursor(5, 0);
	    lcd.print("PAUSED");
    }

	lcd.setCursor(15, 0);
    lcd.print(lives);
    lcd.setCursor(15 - (int) log10(score), 1);
    lcd.print(score);
    
    if (start_pressed() || red_pressed()) mode = game; // Resume game
    else if (blue_pressed()) mode = start_menu; // Reset game to start menu
    
    delay(10);
}

void debug_1_loop() {
    lcd.clear();

    if (red_pressed()) tone(speaker_pin, NOTE_B7, 50);
    if (red_released()) tone(speaker_pin, NOTE_A7, 50);
    if (red_state < 0) lcd.setCursor(7, 0);
    else lcd.setCursor(8, 0);
    lcd.print(red_state);

    if (blue_pressed()) tone(speaker_pin, NOTE_D7, 50);
    if (blue_released()) tone(speaker_pin, NOTE_C7, 50);
    if (blue_state < 0) lcd.setCursor(7, 1);
    else lcd.setCursor(8, 1);
    lcd.print(blue_state);

    if (stick_pressed()) tone(speaker_pin, NOTE_D8, 100);
    if (stick_released()) tone(speaker_pin, NOTE_C8, 50);
    if (stick_state < 0) lcd.setCursor(4, 0);
    else lcd.setCursor(5, 0);
    lcd.print(stick_state);

    lcd.setCursor(0, 0);
    lcd.print(stick_y);
    lcd.setCursor(0, 1);
    lcd.print(stick_x);

    delay(10);

    if (start_pressed()) mode = debug_2; // Millis debug
}

void debug_2_loop() {
    lcd.clear();

    lcd.setCursor(0, 0);
    lcd.print(millis());

	if (start_pressed()) mode = start_menu; // Start screen
	
    delay(10);
}
